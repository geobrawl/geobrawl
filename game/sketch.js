
var Engine = Matter.Engine;
var World = Matter.World;
var Bodies = Matter.Bodies;
var Body = Matter.Body;
var Composites = Matter.Composites;
var keys = [];
var Events = Matter.Events;
var frame = 0;
var seconds = 0;



var engine;
var world;
var circle1;
var ground;


function setup(){
	createCanvas(400, 400);
	engine = Engine.create();
	world = engine.world;
	ground = new Boundary(200, 380, width, 50, 0); // position is calculated from center of object
	circle1 = new Circle(200, 100, 20);
}

function incrementSeconds(){
  seconds += 1;
  console.log(seconds);
  // if (seconds > 0){
  //   document.getElementById('info').innerHTML = "Frame: " + frame;
  // }
}

setInterval(incrementSeconds, 1000);


function draw(){
	background(50);
  Engine.update(engine);
  ground.show();
  circle1.show();

  // Events.on(engine, "collisionStart", function(event) {
  //   var pairs = event.pairs
  // 	for (var i = 0, j = pairs.length; i != j; ++i) {
  //  		var pair = pairs[i];
  //   	if (pair.bodyA.id === circle1.body.id) {
  //     	 circle1.body.ground = true;
  //   	} else if (pair.bodyB.id === circle1.body.id) {
  //    	 circle1.body.ground = true;
  //   	}
  // 	}
	 // });

  Events.on(engine, "collisionActive", function(event) {      // only lets you jump if you're on ground
    var pairs = event.pairs
    for (var i = 0, j = pairs.length; i != j; ++i) {
      var pair = pairs[i];
      if (pair.bodyA.id === circle1.body.id) {
         circle1.body.ground = true;
      } else if (pair.bodyB.id === circle1.body.id) {
       circle1.body.ground = true;
      }
    }
  });

  var maxVelocity = 0.03;

	document.body.addEventListener("keydown", function(e) {
  		keys[e.keyCode] = true;
  		if (keys[87] || keys[38]) {                                   // if 'w' or up is pressed
        if (circle1.body.ground){
    			circle1.body.force = { x: 0, y: -0.05 };	// jump
       	 	circle1.body.ground = false;	// prevents jump spam
        }
  		}   
  		if (keys[65] || keys[37]) {                                   // if 'a' or left is pressed
        if (circle1.body.force.x > -maxVelocity){
          circle1.body.force.x = -0.01; 
        }
  		}
  		if (keys[68] || keys[39]) {                                   // if 'd' or right is pressed
        if (circle1.body.force.x < maxVelocity){
          circle1.body.force.x = 0.01; 
        }
  		}

	});



	document.body.addEventListener("keyup", function(e) {
  		keys[e.keyCode] = false;
	});

  frame ++;

}












