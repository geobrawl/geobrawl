function tabSwitching(event, tab, background, color){
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for(i=0; i< tabcontent.length; i++){
        tabcontent[i].style.display = "none";
    }
    document.getElementById(tab).style.display = "block";
    document.getElementById(background).style.borderColor = color;
}
